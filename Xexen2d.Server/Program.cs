﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xexen2d.Server.Game;
using Xexen2d.Server.Utils;
using Xexen2d.Utils.Packets;

namespace Xexen2d.Server
{
    public class Program
    {
        private static bool _stop = false;
        public static Network Network { get; private set; }
        public static World World { get; private set; }
        public static PacketConverter PacketConverter { get; private set; }
        public static Dictionary<NetPeer, Player> ConnectedPeers { get; private set; }
        public static NetDataWriter Writer { get; set; } = new NetDataWriter();

        public static void Main(string[] args)
        {
            Console.WriteLine("Init server...");
            _initServer();
            Console.WriteLine("Initialized!");
            Console.WriteLine("Starting server...");
            _startServer();
            Console.WriteLine("Server started!");
            _consoleInput();
        }

        private static void _consoleInput()
        {
            while (!_stop)
            {
                var str = Console.ReadLine();
            }
        }

        private static void _startServer()
        {
            Network.Listener.PeerConnectedEvent += Listener_PeerConnectedEvent;
            Network.Listener.PeerDisconnectedEvent += Listener_PeerDisconnectedEvent;
            Network.Listener.NetworkReceiveEvent += Listener_NetworkReceiveEvent;
            Network.Serializer.SubscribeReusable<LoginRequest, NetPeer>(OnLoginRequestRecieved);
            Network.Serializer.SubscribeReusable<ControlStatePacket, NetPeer>(OnControlStatePacketRecieved);
            Network.Serializer.SubscribeReusable<PositionPacket, NetPeer>(OnPositionPacketRecieved);
            Network.Serializer.SubscribeReusable<DisconnectPacket, NetPeer>(OnDisconnectPacketRecieved);
            if (!Network.Start())
            {
                Console.WriteLine("Server start failed!");
            }
        }

        private static void OnDisconnectPacketRecieved(DisconnectPacket packet, NetPeer peer)
        {
            if (ConnectedPeers.ContainsKey(peer))
            {
                packet.ID = peer.ConnectId;
                Network.Serializer.Serialize(Writer, packet);
                SendToAnyoneInGame(Writer, SendOptions.ReliableOrdered);
                Writer.Reset();
                ConnectedPeers.Remove(peer);
            }
            Network.Server.DisconnectPeer(peer);
        }

        private static void OnPositionPacketRecieved(PositionPacket packet, NetPeer peer)
        {
            if (ConnectedPeers.ContainsKey(peer))
            {
                packet.ID = peer.ConnectId;
                Network.Serializer.Serialize(Writer, packet);
                SendToAnyoneInGame(Writer, SendOptions.ReliableOrdered);
                Writer.Reset();
                return;
            }
            Network.Server.DisconnectPeer(peer);
        }

        private static void OnLoginRequestRecieved(LoginRequest pack, NetPeer peer)
        {
            foreach (var pir in ConnectedPeers)
            {
                if (pir.Key.Equals(peer))
                    continue;
                Network.Serializer.Serialize(Writer, new LoginResult() { ID = pir.Key.ConnectId, Yourself = false });
                //Network.Serializer.Serialize(Writer, new PositionPacket()
                //{
                //    ID = pir.Key.ConnectId,
                //    X =
                //})
            }
            Network.Serializer.Serialize(Writer, new LoginResult() { ID = peer.ConnectId, Yourself = true });
            peer.Send(Writer, SendOptions.ReliableOrdered);
            Writer.Reset();
            Network.Serializer.Serialize(Writer, new LoginResult() { ID = peer.ConnectId, Yourself = false });
            SendToAnyOtherInGame(peer, Writer, SendOptions.ReliableOrdered);
            Writer.Reset();
        }

        private static void Listener_NetworkReceiveEvent(NetPeer peer, NetDataReader reader)
        {
            //Console.WriteLine($"Packet from {peer.ConnectId} recieved.");
            //Console.WriteLine($"Packet length = {reader.Data.LongLength}");
            Network.Serializer.ReadAllPackets(reader, peer);
            reader.Clear();
        }

        private static void SendToAnyOtherInGame(NetPeer peer, NetDataWriter writer, SendOptions opts)
        {
            foreach (var pir in ConnectedPeers)
            {
                if (peer.Equals(pir.Key))
                    continue;
                pir.Key.Send(Writer, SendOptions.ReliableOrdered);
                //Console.WriteLine($"Sending packet to {pir.Key.ConnectId}. Connected peers count = {ConnectedPeers.Count}.");
            }
        }

        private static void SendToAnyoneInGame(NetDataWriter writer, SendOptions opts)
        {
            foreach (var pir in ConnectedPeers)
            {
                pir.Key.Send(Writer, SendOptions.ReliableOrdered);
                //Console.WriteLine($"Sending packet to {pir.Key.ConnectId}. Connected peers count = {ConnectedPeers.Count}.");
            }
        }

        private static void OnControlStatePacketRecieved(ControlStatePacket packet, NetPeer peer)
        {
            if (ConnectedPeers.ContainsKey(peer))
            {
                packet.ID = peer.ConnectId;
                //Console.WriteLine($"Sending controlstate of {packet.ID}");
                Network.Serializer.Serialize(Writer, packet);
                SendToAnyoneInGame(Writer, SendOptions.ReliableOrdered);
                Writer.Reset();
                return;
            }
            Network.Server.DisconnectPeer(peer);
        }

        private static void _initServer()
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            ConnectedPeers = new Dictionary<NetPeer, Player>();
            Network = new Network();
            World = new World();
            PacketConverter = new PacketConverter(World);
        }

        private static void Listener_PeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Console.WriteLine($"Disconnected {peer.EndPoint.Host}:{peer.EndPoint.Port}");
            OnDisconnectPacketRecieved(new DisconnectPacket() { ID = peer.ConnectId, Message = disconnectInfo.Reason.ToString() }, peer);
        }

        private static void Listener_PeerConnectedEvent(NetPeer peer)
        {
            Console.WriteLine($"Recieved connection from {peer.EndPoint.Host}:{peer.EndPoint.Port}");
            if (!ConnectedPeers.ContainsKey(peer))
            {
                ConnectedPeers.Add(peer, new Player());
            }
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            _stop = true;
            Console.WriteLine("Stopping server...");
            Network.Stop();
            Console.WriteLine("Server stopped. Press any key to quit...");
        }
    }
}