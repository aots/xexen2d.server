﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Xexen2d.Server.Game
{
    public enum PeerState
    {
    }

    public class Player
    {
        public string Name { get; set; }
        public Vector2 Position { get; set; }

        public Player()
        {
        }
    }
}