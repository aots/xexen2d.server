﻿using Xexen2d.Server.Game;

namespace Xexen2d.Server.Utils
{
    public class PacketConverter
    {
        public World World { get; private set; }

        public PacketConverter(World world)
        {
            World = world;
        }
    }
}