﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Xexen2d.Server
{
    public class Network
    {
        public EventBasedNetListener Listener { get; set; } = new EventBasedNetListener();

        public NetManager Server { get; set; }

        public NetSerializer Serializer { get; set; }

        public int Port { get; set; } = 1488;

        private Thread _thread;
        private bool _stop = false;

        public Network()
        {
            Server = new NetManager(Listener, 8 /* maximum clients */, "Xexen2d");
            Serializer = new NetSerializer();
            _thread = new Thread(_update);
            _thread.IsBackground = true;
        }

        public bool Start()
        {
            if (!Server.Start(Port))
                return false;
            _thread.Start();
            return true;
        }

        public void Stop()
        {
            _stop = true;
            _thread.Join();
            Server.Stop();
        }

        private void _update()
        {
            while (!_stop)
            {
                Server.PollEvents();
            }
        }
    }
}